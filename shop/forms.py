from django.forms import *

from shop.models import Comments


class CommentsForm(ModelForm):
    class Meta:
        model = Comments
        fields = ("title", "body", "stars")
        widgets = {
            "title": TextInput(
                attrs={"class": "form-control", "placeholder": "title"}
            ),
            "body": TextInput(
                attrs={"class": "form-control", "placeholder": "body"}
            ),
            "stars": Select(
                attrs={"class": "form-control"}
            )
        }

    def clean(self):
        if len(self.cleaned_data["title"]) > 90:
            raise ValidationError("title can't be more then 90 simbols")
        if len(self.cleaned_data["body"]) > 1000:
            raise ValidationError("body can't be more then 90 simbols")
        print(self.cleaned_data)
        return self.cleaned_data


star_list = [("all", "all"), (5, "five"), (4, "four"), (3, "three"), (2, "two"), (1, "own"), (0, "zero")]


class FilterForm(Form):
    MinPrice = IntegerField(label="Min price", required=False)
    MaxPrice = IntegerField(label="Max price", required=False)
    Stars = ChoiceField(label="Stars", choices=star_list, required=False)
    MinBought = IntegerField(label="Min bought", required=False)
    MaxBought = IntegerField(label="Max bought", required=False)
    IsAvalible = BooleanField(label="Is avalible", required=False)

