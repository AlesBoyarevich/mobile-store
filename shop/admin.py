from django.contrib import admin
from shop.models import *


class AdminPhones(admin.ModelAdmin):
    list_display = ["name", "price", "stars", "bought", "description"]


class AdminImages(admin.ModelAdmin):
    list_display = ["img"]


admin.site.register(Phones,)
admin.site.register(Images,)
admin.site.register(Comments,)
admin.site.register(Bucket,)
