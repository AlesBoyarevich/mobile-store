from django import template

register = template.Library()


@register.filter(name="star")
def star(value, arg):
    return ('*' * value) + ('0' * (5 - value))
