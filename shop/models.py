from django.db.models import *
from account.models import User

star_list = [(1, "own"), (2, "two"), (3, "three"), (4, "four"), (5, "five"), (0, "zero")]


class Images(Model):
    img = ImageField()

    def __str__(self):
        return self.img.url


# Create your models here.
class Phones(Model):
    def image_path(self, filename):
        ext = filename.split(".")[-1]

    name = CharField(max_length=255)
    price = IntegerField()
    images = ManyToManyField(Images)
    main_image = ForeignKey(Images, on_delete=SET_NULL, null=True, related_name="images_main_image", )
    stars = IntegerField(choices=star_list, default=0)
    bought = BigIntegerField(default=0)
    description = CharField(
        max_length=500,
        default="""No description. If this is a problem, <a href={% url 'contact us' %}>contact us on this url</a>""",
    )

    def __str__(self):
        return f"{self.name}({self.id})"


class Comments(Model):
    phone = ForeignKey(Phones, on_delete=CASCADE)
    author = ForeignKey(User, default="ghost", on_delete=SET_DEFAULT)
    title = CharField(max_length=90)
    body = TextField(max_length=1000)
    stars = IntegerField(choices=star_list, default=0)
    created = DateTimeField(auto_now_add=True, verbose_name="created time")
    updated = DateTimeField(auto_now=True, verbose_name="last update")


class Bucket(Model):
    user = ForeignKey(User, on_delete=CASCADE)
    phones = ManyToManyField(Phones)

    def __str__(self):
        return str(self.user)
