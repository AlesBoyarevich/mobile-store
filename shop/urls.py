from shop.views import *
from django.urls import path

urlpatterns = [
    path("", ShopView.as_view(), name="shop"),
    path("detail/<int:phone_pk>", ProductView.as_view(), name="detail"),
    path("bucket/", BucketView.as_view(), name="bucket"),
    path("bucket/delete/<int:phone_pk>", bucket_delete_view, name="bucket_delete"),
    path("bucket/create/<int:phone_pk>", bucket_create_view, name="bucket_create"),
]
