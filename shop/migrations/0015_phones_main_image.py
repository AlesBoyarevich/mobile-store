# Generated by Django 4.2.1 on 2023-09-13 19:08

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("shop", "0014_remove_images_ismain"),
    ]

    operations = [
        migrations.AddField(
            model_name="phones",
            name="main_image",
            field=models.ImageField(null=True, upload_to=""),
        ),
    ]
