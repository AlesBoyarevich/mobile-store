# Generated by Django 4.2.1 on 2023-09-01 12:21

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("shop", "0005_phones_avalibale_alter_phones_bought"),
    ]

    operations = [
        migrations.RenameField(
            model_name="phones",
            old_name="avalibale",
            new_name="available",
        ),
    ]
