from django.core.paginator import Paginator
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import *

from shop.forms import *
from shop.models import *
from core.utils import DataMixin


class ShopView(TemplateView, DataMixin):
    # ToDo fix that phones can see only logined users
    Phones = Phones.objects.all()
    template_name = "shop/brand.html"
    form = FilterForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(
            title="Catalog",
            page_tag="shop",
            form=self.form,
            Phones=self.Phones,
        )
        return dict(list(context.items()) + list(c_def.items()))

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        min_price = request.GET.get("MinPrice")
        max_price = request.GET.get("MaxPrice")
        stars = request.GET.get("Stars")
        min_bought = request.GET.get("MinBought")
        max_bought = request.GET.get("MaxBought")
        is_avalible = request.GET.get("IsAvalible")

        if not min_price:
            min_price = 0
        if not max_price:
            max_price = 10000000
        if not stars:
            stars = "all"
        if stars != "all":
            context["Phones"] = context["Phones"].filter(stars=stars)
        if not min_bought:
            min_bought = 0
        if not max_bought:
            max_bought = 1000000

        context["Phones"] = context["Phones"].filter(
            price__gte=min_price,
            price__lte=max_price,
            bought__gte=min_bought,
            bought__lte=max_bought)
        if is_avalible:
            context["Phones"] = context["Phones"].filter(price__gt=0)
        if not context["Phones"]:
            context["Phones"] = 0
        if context["Phones"] != 0:
            paginator = Paginator(context["Phones"], 9)
            page_number = request.GET.get("page")
            page_object = paginator.get_page(page_number)
            context["page_object"] = page_object

        form = self.form
        context["form"] = form(initial={
            "MinPrice": min_price,
            "MaxPrice": max_price,
            "Stars": stars,
            "MinBought": min_bought,
            "MaxBought": max_bought,
            "IsAvalible": is_avalible
        })
        return self.render_to_response(context)


class ProductView(TemplateView, DataMixin):
    success_url = reverse_lazy("index")
    template_name = "shop/product.html"
    form = CommentsForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        phone_pk = kwargs.get("phone_pk")
        phone = get_object_or_404(Phones, pk=phone_pk)
        images = phone.images.all()
        comments = Comments.objects.filter(phone=phone.id)
        paginator = Paginator(comments, 2)
        page_number = self.request.GET.get("page")
        page_object = paginator.get_page(page_number)
        if self.request.user.is_authenticated:
            is_write = comments.all().filter(author=self.request.user)
        else:
            is_write = None
        print(is_write)
        c_def = self.get_user_context(
            title="Catalog",
            page_tag="shop",
            phone=phone,
            images=list(enumerate(images)),
            phone_pk=phone_pk,
            page_object=page_object,
            form=self.form(),
            is_write=is_write,
        )
        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):

        form = self.form(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            phone = Phones.objects.get(pk=kwargs.get("phone_pk"))
            data["phone"] = phone
            data["author"] = request.user
            print(type(request.user))
            coment = Comments(**data).save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class BucketView(TemplateView, DataMixin):
    template_name = "shop/bucket.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        if self.request.user.is_authenticated:
            try:
                bucket = get_object_or_404(Bucket, user=self.request.user)
            except:
                bucket = Bucket(user=self.request.user).save()
            context["bucket"] = bucket
        else:
            context["bucket"] = None
        c_def = self.get_user_context(
            title="bucket",
        )
        return dict(list(context.items()) + list(c_def.items()))


def bucket_delete_view(request, phone_pk):
    bucket = get_object_or_404(Bucket, user=request.user)
    bucket.phones.remove(phone_pk)
    return redirect("bucket")


def bucket_create_view(request, phone_pk):
    bucket = get_object_or_404(Bucket, user=request.user)
    bucket.phones.add(phone_pk)
    return redirect(reverse_lazy("detail", kwargs={'phone_pk': phone_pk}))
