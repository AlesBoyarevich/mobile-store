from django.urls import path
from core.views import *

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("about/", AboutView.as_view(), name="about"),
    path("contact_us/", ContactUsView.as_view(), name="contact us"),
]
