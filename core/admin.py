from django.contrib import admin
from core.models import ContactUsModel


# Register your models here.
class AdminFeedback(admin.ModelAdmin):
    list_display = ("username", "status", "updated")


admin.site.register(ContactUsModel, AdminFeedback)
