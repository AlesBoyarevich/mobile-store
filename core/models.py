from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class ContactUsModel(models.Model):
    Statuses = [("N", "New feedback"), ("P", "Feedback in proces"), ("F", "feedback was finished")]

    username = models.ForeignKey(User, on_delete=models.CASCADE)
    email = models.EmailField(verbose_name="user email")
    phone = models.CharField(max_length=15, verbose_name="mobile phone")
    message = models.TextField(verbose_name="user message")
    status = models.CharField(max_length=1, default="N")
    created = models.DateTimeField(auto_now_add=True, verbose_name="created time")
    updated = models.DateTimeField(auto_now=True, verbose_name="last update")

    class Meta:
        db_table = "feedback"
        verbose_name = "feed back"
        verbose_name_plural = "feedbacks"

    def __str__(self):
        return f"{self.id} {self.username}"

