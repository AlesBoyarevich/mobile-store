import re

from django import forms
from django.contrib.auth.models import User
from django.forms import *

from core.models import ContactUsModel

Statuses = [("N", "New feedback"), ("P", "Feedback in proces"), ("F", "feedback was finished")]


class ContactUsForm(ModelForm):
    class Meta:
        model = ContactUsModel
        fields = ("email", "phone", "message")
        widgets = {
            "email": EmailInput(attrs={"class": "form-control", "placeholder": "Your email", "name": "Your Name"}),
            "phone": TextInput(attrs={"class": "form-control", "placeholder": "Your phone", "name": "Your Name"}),
            "message": Textarea(attrs={"class": "form-control", "placeholder": "Your message", "name": "Your Name"}),
        }

    def clean_phone(self):
        data = self.cleaned_data["phone"]
        if not re.match(r"^\+\d{7,12}$", data):
            raise forms.ValidationError("Phone number havs to be digits")
        return data

    def clean_message(self):
        data = self.cleaned_data["message"]
        if not data:
            raise forms.ValidationError("invalid message")
        return data
