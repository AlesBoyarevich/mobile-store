from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView

from core.forms import ContactUsForm
from django.contrib import messages

from core.models import ContactUsModel
from core.utils import DataMixin
from shop.models import *


# Create your views here.
class IndexView(DataMixin, TemplateView):
    template_name = 'core/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(
            title="Home",
            phones=Phones.objects.all()[0:6],
            page_tag="home"
        )
        return dict(list(context.items()) + list(c_def.items()))


class AboutView(DataMixin, TemplateView):
    template_name = "core/about.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(
            title="About",
            page_tag="about"
        )
        return dict(list(context.items()) + list(c_def.items()))


class ContactUsView(DataMixin, CreateView):
    template_name = "core/contact.html"
    model = ContactUsModel
    form_class = ContactUsForm
    success_url = reverse_lazy("index")

    def get(self, request, *args, **kwargs):
        self.object = None
        context = self.get_context_data()
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(request.POST)
        if form.is_valid():
            contact = form.save(commit=False)
            contact.username = User.objects.get(username=request.user)
            form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        form = self.get_form_class()
        c_def = self.get_user_context(
            title="Contact us",
            page_tag="contact_us",
            form=form(initial={"phone": "+"})
        )
        return dict(list(context.items()) + list(c_def.items()))
