from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect


class DataMixin:
    success_url = None
    contact_phones = ["+375255005893"]

    def get_user_context(self, **kwargs):
        context = kwargs
        context.update(
            {
                "contact_phones": self.contact_phones,
                "contact_email": "alesboyarevich@gmail.com",
            }
        )
        return context

    def get_success_url(self):
        """Return the URL to redirect to after processing a valid form."""
        if not self.success_url:
            raise ImproperlyConfigured("No URL to redirect to. Provide a success_url.")
        return str(self.success_url)  # success_url may be lazy

    # def form_valid(self):
    #     """If the form is valid, redirect to the supplied URL."""
    #     return HttpResponseRedirect(self.get_success_url())
    #
    # def form_invalid(self, form):
    #     """If the form is invalid, render the invalid form."""
    #     return self.render_to_response(self.get_context_data(form=form))
