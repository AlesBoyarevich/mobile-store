from django.urls import path
from account.views import *

urlpatterns = [
#     path("registration/", RegistrationView.as_view(), name="registration_page"),
    path("login/", UserLoginView.as_view(), name="login"),
    path("logout/", UserLogoutView.as_view(), name="logout"),
#     path("profile/", profile_view, name="profile")
    path("registration/", RegistrationView.as_view(), name="registration")
]
