from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import *
# from account.forms import RegistrationForm
from django.contrib.auth.views import *

from account.forms import RegistrationForm, CustomLoginForm
from core.utils import DataMixin


class RegistrationView(DataMixin, CreateView):
    template_name = "account/create.html"
    model = User
    form_class = RegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        form_data = form.cleaned_data
        form_data.pop("password2")
        user = self.model(**form_data)
        user.password = make_password(form_data.get("password"))
        user.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(
            title="Registration",
            page_tag="registration"
        )
        return dict(list(context.items()) + list(c_def.items()))


class UserLoginView(LoginView, DataMixin):
    form_class = CustomLoginForm
    template_name = 'account/login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        c_def = self.get_user_context()
        return dict(list(context.items()) + list(c_def.items()))
    #
    # def get_success_url(self):
    #     return self.request.GET.get("next")


class UserLogoutView(LogoutView, DataMixin):
    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        c_def = self.get_user_context()
        return dict(list(context.items()) + list(c_def.items()))

    def get_success_url(self):
        return self.request.GET.get("next")
