from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.forms import *
from django.contrib.auth.models import User


class RegistrationForm(ModelForm):
    password2 = CharField(label="repeat password", widget=PasswordInput(attrs={"class": "form-control", "placeholder": "repeat password"}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')
        widgets = {
            "username": TextInput(
                attrs={"class": "form-control", "placeholder": "username"}
            ),
            "first_name": TextInput(
                attrs={"class": "form-control", "placeholder": "first name"}
            ),
            "last_name": TextInput(
                attrs={"class": "form-control", "placeholder": "last name"}
            ),
            "email": EmailInput(
                attrs={"class": "form-control", "placeholder": "email"}
            ),
            "password": PasswordInput(
                attrs={"class": "form-control", "placeholder": "password"}
            ),
            "password2": PasswordInput(
                attrs={"class": "form-control", "placeholder": "repeat password"}
            )
        }

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']


class CustomLoginForm(AuthenticationForm):
    username = UsernameField(widget=TextInput(attrs={"class": "form-control", "placeholder": "username", "autofocus": True}))
    password = CharField(
        label="Password",
        strip=False,
        widget=PasswordInput(attrs={"class": "form-control", "placeholder": "password", "autocomplete": "current-password"}),
    )
